<?php

Route::group(['namespace' => 'Service'], function() {
	Route::get('/cek-kabupaten', 'RajaOngkirController@cekKabupaten');
	Route::post('/cek-ongkir', 'RajaOngkirController@cekOngkir');
});

Route::group(['namespace' => 'Frontend'], function(){
	Route::get('/', 'MainController@index')->name('frontend.index');
	Route::get('/produk/{slug}', 'MainController@show')->name('frontend.show');
	Route::get('/jenis-produk/{slug}', 'MainController@showJenisProduk')->name('frontend.show.jenis.produk');
	Route::get('/search', 'MainController@search')->name('frontend.search');

	//Cart
	Route::get('cart', 'CartController@index')->name('frontend.cart.index');
	Route::post('cart', 'CartController@store')->name('frontend.cart.store');
	Route::patch('/cart/{product}', 'CartController@update')->name('frontend.cart.update');
	Route::delete('/cart/{product}', 'CartController@destroy')->name('frontend.cart.destroy');

	//Checkout
	Route::get('/checkout', 'CheckoutController@index')->name('frontend.checkout');
	Route::post('/checkout/store', 'CheckoutController@store')->name('frontend.checkout.store');

	//Informasi Pembayaran
	Route::get('/informasi-pembayaran/{id}', 'CheckoutController@infoPembayaran')->name('frontend.infopembayaran');
});

Auth::routes();
Route::namespace('Auth')->group(function () {
	Route::post('/login', 'LoginController@postLogin');
	Route::post('/login/user', 'LoginController@postLoginUser')->name('login.user');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace' => 'Backend', 'middleware' => 'auth', 'prefix' => 'backend'], function(){
	Route::group(['prefix' => 'penjual'], function(){
		Route::get('/', 'PenjualController@index')->name('penjual.index');
		Route::post('/store', 'PenjualController@store')->name('penjual.store');
		Route::post('{id}/update/', 'PenjualController@update')->name('penjual.update');
		Route::get('/destroy/{id}', 'PenjualController@destroy')->name('penjual.destroy');
	});

	Route::group(['prefix' => 'jenis'], function(){
		Route::get('/', 'JenisController@index')->name('jenis.index');
		Route::post('/store', 'JenisController@store')->name('jenis.store');
		Route::post('{id}/update/', 'JenisController@update')->name('jenis.update');
		Route::get('/destroy/{id}', 'JenisController@destroy')->name('jenis.destroy');
	});

	Route::group(['prefix' => 'produk'], function(){
		Route::get('/', 'ProdukController@index')->name('produk.index');
		Route::get('/create', 'ProdukController@create')->name('produk.create');
		Route::post('/store', 'ProdukController@store')->name('produk.store');
		Route::get('/edit/{id}', 'ProdukController@edit')->name('produk.edit');
		Route::get('/detail-produk/{id}', 'ProdukController@detailproduk')->name('produk.detailproduk');
		Route::post('/detail-produk/store', 'ProdukController@storeDetailProduk')->name('produk.storeDetailProduk');
		Route::get('/detail-produk/destroy/{id}', 'ProdukController@destroyDetailProduk')->name('produk.destroyDetailProduk');
		Route::post('/update/{id}', 'ProdukController@update')->name('produk.update');
		Route::get('/destroy/{id}', 'ProdukController@destroy')->name('produk.destroy');

		Route::get('/kategori/{id}', 'ProdukController@kategori')->name('produk.kategori');
		Route::get('edit/kategori/{id}', 'ProdukController@kategori');
	});

	Route::group(['prefix' => 'pemesanan'], function(){
		Route::get('/', 'PemesananController@index')->name('pemesanan.index');
		Route::get('/create', 'PemesananController@index')->name('pemesanan.create');
		Route::post('/update/{id}', 'PemesananController@update')->name('pemesanan.update');
		Route::get('/destroy/{id}', 'PemesananController@destroy')->name('pemesanan.destroy');
		Route::post('/verifikasi', 'PemesananController@verifikasi')->name('pemesanan.verifikasi');
	});
});

<nav class="navbar navbar-expand-lg navbar-light fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('assets/image/icon2.png') }}" alt="Produk Desa Icon" class="pr-0">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                @foreach($jenisKategoriProduk as $item)
                    <a class="nav-link" style="color:black;" href="{{ route('frontend.show.jenis.produk', $item->slug) }}">
                        {{ $item->name }} 
                    </a>
                @endforeach
                @foreach($jenisPertambakan as $item)
                    <a class="nav-link" style="color:black;" href="{{ route('frontend.show.jenis.produk', $item->slug) }}">
                        {{ $item->name }}
                    </a>
                @endforeach
                @foreach($jenisKreasiDesa as $item)
                    <a class="nav-link" style="color:black;" href="{{ route('frontend.show.jenis.produk', $item->slug) }}">
                        {{ $item->name }}
                    </a>
                @endforeach
                
                <li class="nav-item mr-1" style="width: 200px;"></li>
                <div class="nav-item">
                    @if(!Auth::check())
                    @else
                    <li class="nav-item dropdown mr-2">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ substr(Auth::user()->name, 0,4)  }}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a  class="dropdown-item" 
                                href="{{ route('logout') }}" 
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endif
                </div>
            </ul>
            <form class="form-inline" action="{{ route('frontend.search') }}" method="GET">
                <div class="active-cyan-4">
                    <input class="form-control" name="search" type="text" placeholder="&#xF002  Cari Produk"
                        aria-label="Search" style="font-family: 'Roboto', FontAwesome;">
                </div>
                <button type="submit" class="form-control btn btn-info"> <i class="fa fa-search"></i></button>
            </form>
        </div>
    </div>
</nav>

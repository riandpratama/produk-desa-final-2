<section class="informasi">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <h4 class="mt-4 mb-3">LAYANAN DAN BANTUAN</h4>
                <div class="row">
                    <div class="col-5">
                        <p>Panduan Berbelanja</p>

                    </div>
                    <div class="col-5">
                        <p>Metode Pengiriman</p>
                    </div>
                    <div class="col-2">

                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <h4 class="mt-4 mb-3">INFORMASI TOKO</h4>
                <table class="table table-borderless table-sm">
                    <tr>
                        <td><img src="{{ asset('assets/image/produkDesa/lokasi.png') }}" style="width: 100%"></td>
                        <td>
                            <p>Desa Tanggulrejo, Kecamatan Manyar, Kabupaten Gresik, Provinsi Jawa Timur</p>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="{{ asset('assets/image/produkDesa/gmail.png') }}" style="width: 100%"></td>
                        <td>
                            <p>desatanggulrejo@gmail.com</p>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="{{ asset('assets/image/produkDesa/telephone.png') }}" style="width: 100%"></td>
                        <td>
                            <p>WA / 085655340653</p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</section>
    
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-md-10 col-lg-6 mt-4">
                <a href="">
                    <img src="{{ asset('assets/image/footer/instagram-logo.png') }}" alt="" class="mr-2">
                </a>
                <a href="">
                    <img src="{{ asset('assets/image/footer/whatsapp-logo.png') }}" alt="" class="mr-2">
                </a>
                <a href="">
                    <img src="{{ asset('assets/image/footer/facebook-logo.png') }}" alt="" class="mr-2">
                </a>
                <a href="">
                    <img src="{{ asset('assets/image/footer/youtube-symbol.png') }}" alt="" class="mr-2">
                </a>
                <a href="">
                    <img src="{{ asset('assets/image/footer/gmail-logo.png') }}" alt="">
                </a>
                <p class="pt-2">Copyright &copy;2020 All rights reserved | Tanggurejo </a></p>
            </div>
        </div>
    </div>
</footer>
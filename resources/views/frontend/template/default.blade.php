<!DOCTYPE html>
<html lang="en">

@include('frontend.template.partials._head')

<body>
		
	@include('frontend.template.partials._nav')
			
	@yield('content')

	@include('frontend.template.partials._footer')

    @include('frontend.template.partials._javascript')
    
</body>

</html>
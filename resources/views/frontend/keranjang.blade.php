@extends('frontend.template.default')

@section('content')
    
    <section class="cart-header mt-5">
        <div class="container text-center">
            <div class="row">
                <div class="col">
                    <h3>Keranjang Anda</h3>
                    <br>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="panel panel-default">
            {{-- <div class="panel-heading"><i class="fa fa-shopping-cart"></i> Keranjang Belanja</div> --}}
            <div class="panel-body">
                <div class="box-body table-responsive no-padding">
                    @if (Session::has('success_message'))
                    <div class="alert alert-success" role="alert">
                        <strong></strong>{{ Session::get('success_message') }}
                    </div>
                    @endif

                    <table id="cart" class="table table-hover table-condensed" style="background-color: white;">
                        <thead>
                            <tr>
                                <th style="width:18%; text-align: center;">Gambar Produk</th>
                                <th style="width:30%; text-align: center;">Nama Produk</th>
                                <th style="width:10%">Harga</th>
                                <th style="width:8%">Stok</th>
                                <th style="width:22%" class="text-center">Subtotal</th>
                                <th style="width:10%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (Cart::content() as $item)
                            <tr>
                                <td data-th="Gambar">
                                    <div class="col-sm-3 hidden-xs"><img src="{{ asset('storages/'.$item->model->image) }}" alt="{{ $item->model->nama_produk }}" class="img-responsive" style="width: 100px; height: 100px;" /></div>
                                </td>
                                <td data-th="Product" style="text-align: center;">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <p class="nomargin" style="margin-left: 30px;">{{ $item->model->nama_produk }}</p>
                                        </div>
                                    </div>
                                </td>
                                <td data-th="Price">Rp. {{ number_format($item->model->harga_jual) }}</td>
                                <td data-th="Quantity">
                                    <select class="quantity" data-id="{{ $item->rowId }}">
                                        @for ($i = 1; $i < $item->model->stok + 1 ; $i++)
                                            <option {{ $item->qty == $i ? 'selected' : '' }}>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </td>
                                <td data-th="Subtotal" class="text-center">Rp. {{ number_format($item->subtotal)  }}</td>
                                <td class="actions" data-th="">
                                    <form action="{{ route('frontend.cart.destroy', $item->rowId) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin ingin menghapus barang dari keranjang?')" ><i class="fa fa-trash-o"></i></button> 
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><a href="{{ url('/') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                                <td colspan="2" class="hidden-xs"></td>
                                <td></td>
                                <td class="hidden-xs text-center"><strong>Total Rp. {{ Cart::subtotal() }}</strong></td>
                                <td><a href="{{ route('frontend.checkout') }}" class="btn btn-success btn-block">Proses <i class="fa fa-angle-right"></i></a></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        (function(){
            const classname = document.querySelectorAll('.quantity')

            Array.from(classname).forEach(function(element) {
                element.addEventListener('change', function() {
                    const id = element.getAttribute('data-id')
                    axios.patch(`/cart/${id}`, {
                        quantity: this.value
                    })
                    .then(function (response) {
                        // console.log(response);
                        window.location.href = '{{ route('frontend.cart.index') }}'
                    })
                    .catch(function (error) {
                        // console.log(error);
                        window.location.href = '{{ route('frontend.cart.index') }}'
                    });
                })
            })
        })();
    </script>

@endsection
@extends('frontend.template.default')

@section('content')

<div id="carouselId" class="carousel slide mt-5" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img src="{{ asset('assets/image/Slider.png') }}" alt="First slide">
        </div>
    </div>
</div>

<section class="produk-desa mt-5 mb-3">
    <div class="container">
        <h3>Spesial Produk Desa</h3>
        <p>Jajanan Khas Desa Tanggulrejo</p>
        <div class="item-produk text-center">
            <div class="row">
                @foreach($populer as $item)
                <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                    <figure class="figure">
                        <div class="figure-img">
                            <img src="{{ asset('storages/'. $item->image) }}" alt="prdouk1" class="img-thumbnail">
                            <a href="{{ route('frontend.show', $item->slug) }}" class="d-flex justify-content-center">
                                <img src="{{ asset('assets/image/produkDesa/badge.png') }}" class="align-self-center"
                                    style="width: 15%">
                            </a>
                        </div>
                        <figcaption class="figure-caption text-center">
                            <h5 class="mb-0">{{ substr($item->nama_produk, 0, 12) }}</h5>
                            <p class="pt-0">Rp. {{ number_format($item->harga_jual,2,',','.') }}</p>
                        </figcaption>
                    </figure>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

<section class="produk-terbaru mb-5">
    <div class="container">
        <h3 class="mb-4">Produk Desa Terbaru</h3>
        <div class="item-produk text-center">
            <div class="row">
                @foreach($news as $item)
                <div class="col-6 col-sm-6 col-md-3 col-lg-3">
                    <figure class="figure">
                        <div class="figure-img">
                            <img src="{{ asset('storages/'. $item->image) }}" alt="prdouk1" style="width: 100%">
                            <a href="{{ route('frontend.show', $item->slug) }}" class="d-flex justify-content-center">
                                <img src="{{ asset('assets/image/produkDesa/badge.png') }}" class="align-self-center"
                                    style="width: 15%">
                            </a>
                        </div>
                        <figcaption class="figure-caption text-center">
                            <h5 class="mb-0">{{ $item->nama_produk }}</h5>
                            <p class="pt-0">Rp. {{ number_format($item->harga_jual,2,',','.') }}</p>
                        </figcaption>
                    </figure>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

@endsection
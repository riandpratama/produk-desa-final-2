@extends('frontend.template.default')

@section('content')

	<section class="cart-header mt-5">
        <div class="container text-center">
            <div class="row">
                <div class="col">
                    <h3>Informasi Pengiriman</h3>
                </div>
            </div>
        </div>
    </section>

    @if (count($errors) > 0)
        <div class="alert alert-danger alert-hidden">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <section class="checkout mt-5">
        <div class="container">
            <form method="POST" action="{{ route('frontend.checkout.store') }}">
            <div class="row justify-content-between" style="margin-bottom: 100px;">
                <div class="col-lg-6">

                    <h4 class="mb-3" style=" font-weight: bold;">Keterangan Pembeli</h4>
                    @if (!Auth::check())
                    <!-- If -->
                    <div class="form-group">
                        <label for="nama">Nama Lengkap</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama') }}" placeholder="Nama Lengkap" required>
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="Email" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="nohp">Password</label>
                        <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" placeholder="Password" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="nohp">Nomor Handphone</label>
                        <input type="number" class="form-control" id="nohp" name="nohp" value="{{ old('nohp') }}" placeholder="08xxx" required>
                    </div>
                    @else
                    <!-- Else -->
                    <div class="form-group">
                        <label for="nama">Nama Lengkap</label>
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ Auth::user()->name }}" readonly="">
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" value="{{ Auth::user()->email }}" readonly="">
                    </div>
                    
                    <div class="form-group">
                        <label for="nohp">Nomor Handphone</label>
                        <input type="number" class="form-control" id="nohp" name="nohp" value="{{ Auth::user()->no_hp }}" readonly="">
                    </div>
                    <input type="hidden" name="password" value="">
                    @endif
                    <!-- End If -->
                    <div class="form-group">
                    	<label for="">Provinsi</label>
                    	@if ($data['rajaongkir']['results'] == NULL) 
                            <p style="color: red; font-size: 12px;"><i>Koneksi buruk harap cek koneksi anda, atau refresh halaman.</i></p>
                        @else
                        <select name="provinsi_id" id="provinsi" class="form-control">
                            <option selected="" disabled="">Pilih Provinsi</option>
                            @foreach($data['rajaongkir']['results'] as $item)
                            <option value="{{ $item['province_id'] }}" data-id="{{ $item['province'] }}">{{ $item['province'] }}</option>
                            @endforeach
                        </select>
                        @endif
                        <input type="hidden" id="provinsiName" name="provinsi">
                    </div>
                    <div class="form-group">
                        <label for="address2">Kota/Kabupaten</label>
                        <select id="kabupaten" class="form-control" name="kabupaten_id"></select>
                        <input type="hidden" id="kabupatenName" name="kabupaten">
                    </div>
                    <div class="form-group">
                        <label for="address2">Alamat Lengkap (Sertakan Kecamatan/Desa)</label>
                        <textarea type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat"></textarea>
                    </div>

                    <table class="table "> 
                        <tr>
                            <td><div class="col-md-12"><strong>Jasa Pengiriman</strong></div></td>
                            <td>:</td>
                            <td>
                                <input type="radio" name="kurir" value="jne"> <img src="{{ asset('assets/kurir/jne.jpeg') }}" width="70px" height="50px"><br><br>
                                <input type="radio" name="kurir" value="tiki"> <img src="{{ asset('assets/kurir/tiki.jpg') }}" width="70px" height="50px"><br><br>
                                <input type="radio" name="kurir" value="pos"> <img src="{{ asset('assets/kurir/pos.jpeg') }}" width="70px" height="50px"><br>
                            </td>
                            <input type="hidden" name="asal" value="133" id="asal" >
                            <input type="hidden" name="berat" value="500" id="berat" >
                        </tr>
                        <tr>
                            <td colspan="3"><button type="button" class="btn btn-success btn-block" id="cek"> <i class="fa fa-search"></i> Cek Ongkir terlebih dahulu</button></td>
                        </tr>
                    </table>
                    <table  class="table table-hover" id="ongkir"></table>

                    <hr>
                    <div class="panel panel-info">
                        
                        <div class="panel-body">
                            <h4 class="mb-3" style=" font-weight: bold;">Pilih pembayaran melalui</h4>
                            <table class="table"> 
                                <tr>
                                    <td width="10%"> Bank</td>
                                    <td>:</td>
                                    <td>
                                        <input type="radio" name="bank" value="bca"> &nbsp;&nbsp; <img src="{{ asset('assets/bank/1.jpg') }}" width="70px" height="50px"> &nbsp;&nbsp;<br>
                                        <input type="radio" name="bank" value="mandiri">&nbsp;&nbsp; <img src="{{ asset('assets/bank/2.jpg') }}" width="70px" height="50px"> &nbsp;&nbsp;<br><br>
                                        
                                        
                                        <input type="radio" name="bank" value="lain">&nbsp;&nbsp;Pilih Bank Lainnya<br>
                                    </td>
                                    <td>
                                        <input type="radio" name="bank" value="bri">&nbsp;&nbsp; <img src="{{ asset('assets/bank/3.jpeg') }}" width="70px" height="50px"> &nbsp;&nbsp;<br>
                                        <input type="radio" name="bank" value="bni">&nbsp;&nbsp; <img src="{{ asset('assets/bank/4.jpg') }}" width="70px" height="50px"> &nbsp;&nbsp;<br><br>
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>

                <!-- ========================================================================= -->
                <div class="col-lg-5">
                    <div class="card checkout-detail">
                        <div class="card-body">
                            <h5 class="card-title">Informasi Biaya</h5>
                            @foreach (Cart::content() as $item)
			                    <div class="row mb-4">
			                        <div class="col-2">
			                            <img src="{{ $item->model->image }}" width="100%">
			                        </div>
			                        <div class="col-6">
			                            <h5 class="m-0">{{ $item->model->nama_produk }}</h5>
			                            <input type="hidden" name="produk_id[]" value="{{ $item->model->id }}" >
			                        	<input type="hidden" name="total_barang" value="{{ Cart::count() }}" >
			                        	<input type="hidden" name="total_pesan" value="{{ Cart::subtotal(0,'.','') }}" >
                                        <input type="hidden" name="harga_total_barang[]" value="{{ $item->subtotal }}" >
			                            <p class="m-0" style="color:#B7B7B7;">Rp {{ number_format($item->subtotal, 2, ',', '.')  }}</p>
			                        </div>
			                        <div class="col-4">
			                            <span class="mx-2">Jumlah: {{ $item->qty }}</span>
                                        <input type="hidden" name="jumlah_barang[]" value="{{ $item->qty }}">
			                        </div>
			                    </div>
			                @endforeach

                            <div class="row mb-3">
                                <div class="col">
                                    <h6 class="m-0">Subtotal</h6>
                                    <small style="color: rgb(148, 148, 148);">{{ Cart::instance('default')->count() }} Total Produk</small>
                                </div>
                                {{-- <div class="col d-flex justify-content-end">
                                    <h6 class="m-0 align-self-center text-success">Rp. {{ Cart::subtotal() }}</h6>
                                </div> --}}
                            </div>

                            <hr>

                            {{-- <div class="row mb-3">
                                <div class="col">
                                    <h6 class="m-0">Courier</h6>
                                    <small style="color: rgb(148, 148, 148);">JNT Express</small>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <h6 class="m-0 align-self-center text-success">IDR 201.000</h6>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col">
                                    <h6 class="m-0">Tax</h6>
                                    <small style="color: rgb(148, 148, 148);">Negara 20%</small>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <h6 class="m-0 align-self-center text-success">IDR 1.799.000</h6>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col">
                                    <h6 class="m-0">Eid Promo</h6>
                                    <small style="color: rgb(148, 148, 148);">10% OFF</small>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <h6 class="m-0 align-self-center text-danger">-IDR 50.000.000</h6>
                                </div>
                            </div> --}}

                            <div class="row mb-3">
                                <div class="col">
                                    <h6 class="m-0">Total Harga</h6>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <h6 class="m-0 align-self-center text-primary">Rp. {{ Cart::subtotal() }}</h6>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- ============================================================================================ -->
                    <div class="row mt-3">
                        <div class="col">
                            <a href="{{ route('frontend.cart.index') }}" style="color: #ADADAD; text-decoration: none;">
                            <button type="button" class="btn btn-block"
                                style="background-color: #EAEAEF; font-weight: bold;">Batal</button></a>
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-block text-white"
                                style="background-color: #28BFDB; font-weight: bold; text-decoration: none;"
                                data-toggle="modal" data-target="#checkoutModal" onclick="return confirm('Lanjutkan pemesanan?')">
                                Lanjut Pembayaran
                            </button>
                        </div>
                    </div>

                </div>
            </div>
            </form>
        </div>
        </section>
        <!-- Akhir Checkout -->


@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    var baseUrl = '{{ url('/') }}';

    $(document).ready(function(){
        $('#provinsi').change(function(){
 
            var prov = $('#provinsi').val();
            var provName = $('#provinsi').find(':selected').attr('data-id')
            
            $('#provinsiName').attr("value", provName);
            
            
            $.ajax({
                type : 'GET',
                url : baseUrl + '/cek-kabupaten',
                data :  'prov_id=' + prov,
                    success: function (data) {
                    
                    $("#kabupaten").html(data);

                    var kabName = $('#kabupaten').find(':selected').attr('data-id')
                    
                    $('#kabupatenName').attr("value", kabName);
                    console.log(kabName)

                    $('#kabupaten').change(function(){
                        var kabName = $('#kabupaten').find(':selected').attr('data-id')
                        $('#kabupatenName').attr("value", kabName);
                        console.log(kabName)
                    });

                }
            });
        });
        
           $("#cek").click(function() {

            var asal = $('#asal').val();
            var kab = $('#kabupaten').val();
            var kurir = $('input[name=kurir]:checked').val();
            var berat = $('#berat').val();
 
            $.ajax({
                type : 'POST',
                url : baseUrl + '/cek-ongkir',
                data :  {
                        'kab_id' : kab, 
                        'kurir' : kurir, 
                        'asal' : asal, 
                        'berat' : berat, 
                    },
                success: function (data) {
                    var response = JSON.parse(data);
                    // console.log(response)

                    var costs = response.rajaongkir.results[0].costs;

                    if (costs.length > 0) {
                        $('#ongkir').html('');
                        for (a=0; a < costs.length; a++ ){

                            description = response.rajaongkir.results[0].costs[a].description;
                            service = response.rajaongkir.results[0].costs[a].service;
                            cost = response.rajaongkir.results[0].costs[a].cost[0];
                            etd = response.rajaongkir.results[0].costs[a].cost[0].etd;
                            value = response.rajaongkir.results[0].costs[a].cost[0].value;

                            var el = $("<tr><td><div class='col-md-12'><strong>"+ service +" - "+ description +" / "+ etd +" hari </strong><input type='hidden' name='service_kurir' value='"+ service +' '+ etd + " hari'></div></td><td>:</td><td> <input type='radio' name='ongkos_kirim' value='"+ value +"'> &nbsp; Rp. "+ value +" <br><br></td></tr>");

                            $("#ongkir").append(el);

                            el = '';
                        }
                    } else {
                        $('#ongkir').html('');
                    } 
                }
            });
        });
    });
</script>

@endsection
@extends('frontend.template.default')

@section('content')
    <section class="cart-header mt-5 pt-5">
        <div class="container text-center">
            <div class="row">
                <div class="col justify-content-center">
                    <img src="image/shopping-cart.png"
                        class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}"
                        alt="">
                    <div class="text-center">
                        <h2>Pembelian Produk Telah Berhasil</h2>
                        <h5 class="mt-3">Anda dapat membayar pesanan melaui transfer ke rekening di bawah ini.
                            Pesanan anda akan kami proses setelah anda mengirimkan bukti transfer
                            ke nomor <span style="color: #23D43A; font-weight: bold;">WhatsApp</span> kami yaitu
                            <span style="color: #23D43A; font-weight: bold;">0856-5534-5534</span> atau dapat langsung
                            <span><a target="_blank" href="https://api.whatsapp.com/send?phone=6285655345534&text=">klik di sini</a></span> untuk langsung terhubung ke WhatsApp. Jangan lupa
                            untuk mencamtukan
                            Nomor Pesanan anda untuk bukti pembayaran barang</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="checkout-pembayaran mt-5">
        <div class="container">
            <div class="row justify-content-between" style="margin-bottom: 50px;">
                <div class="col-lg-6">
                    <h3>Keterangan Rekening</h3>
                    <table class="table table-sm table-borderless mb-5 ">
                        <tbody>
                            <tr>
                                <td>
                                    <h5>A/N</h5>
                                </td>
                                <td>
                                    <H5><b>Muhammad Ibrahim Adham Karim</b></H5>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>Bank</h5>
                                </td>
                                <td>
                                    <h5><b>Mandiri Syariah</b></h5>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>No. Rekening</h5>
                                </td>
                                <td>
                                    <h5><b>12345678910</b></h5>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>Tanggal Pembelian</h5>
                                </td>
                                <td>
                                    <h5><b>{{ date('d F Y', strtotime($pemesanan->tgl_pemesanan)) }}</b></h5>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>No. Pesanan</h5>
                                </td>
                                <td>
                                    <h5><b>{{ $pemesanan->kode_pemesanan }}</b></h5>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>Biaya Transfer</h5>
                                </td>
                                <td>
                                    <h5><b>Rp. {{ number_format($pemesanan->bayar_unik,2,',','.') }}</b></h5>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p style="color:red;"><i>*Transfer sesuai total pembayaran anda, agar memudahkan pihak admin untuk mengkonfirmasi pembayaran Anda.</i></p>
                </div>

                <!-- ========================================================================= -->
                <div class="col-lg-5">
                    <div class="card checkout-detail">
                        <div class="card-body">
                            <h5 class="card-title">Informasi Biaya</h5>
                            @foreach ($pemesanan->pemesanandetail as $item)
			                    <div class="row mb-4">
			                        <div class="col-2">
			                            <img src="{{ asset('storages/'.$item->produk->image) }}" width="100%">
			                        </div>
			                        <div class="col-6">
			                            <h5 class="m-0">{{ $item->produk->nama_produk }}</h5>
			                            <input type="hidden" name="produk_id[]" value="{{ $item->id }}" >
			                        	<input type="hidden" name="total_barang" value="" >
			                        	<input type="hidden" name="total_pesan" value="" >
			                            <p class="m-0" style="color:#B7B7B7;">Rp {{ number_format($item->harga_total_barang, 2, ',', '.')  }}</p>
			                        </div>
			                        <div class="col-4">
			                            <span class="mx-2">Jumlah: {{ $item->jumlah_barang }}</span>
                                        <input type="hidden" name="jumlah_barang[]" value="{{ $item->qty }}">
			                        </div>
			                    </div>
			                @endforeach
                            <hr>

                            <div class="row mb-3">
                                <div class="col">
                                    <h6 class="m-0">Total Harga</h6>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <h6 class="m-0 align-self-center text-primary">Rp. {{ number_format($pemesanan->total_pesan, 2, ',', '.') }} </h6>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- ============================================================================================ -->
                </div>
            </div>
        </div>
    </section>
    <!-- Akhir Checkout -->

    <div class="text-center mb-5 mt-0">
        <a href="{{ url('/') }}" style="text-decoration: none; color: white;">
            <button type="button" class="btn btn-lg text-white" style="background-color: #28BFDB; font-weight: bold;"
                data-toggle="modal" data-target="#checkoutModal">Kembali
                ke Halaman Produk
                Desa
            </button></a>
    </div>
@endsection
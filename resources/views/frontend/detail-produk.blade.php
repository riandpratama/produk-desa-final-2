@extends('frontend.template.default')

@section('content')
    
    @if($produk == NULL)
    <div class="container mt-5">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('frontend.index') }}">Home</a></li>
            </ol>
        </nav>
    </div>
    <!-- Akhir Breadcrum -->
    @else
    <!-- Breadcrum -->
    <div class="container mt-5">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('frontend.index') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="">{{ ($produk->jenis == NULL) ? $produk->jenis->kategori : '' }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('frontend.show.jenis.produk', $produk->jenis->slug) }}">{{ $produk->jenis->name }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $produk->nama_produk }}</li>
            </ol>
        </nav>
    </div>
    <!-- Akhir Breadcrum -->
    @endif
    <section class="single-item">
        <div class="container">
            <div class="row">
                @if($produk == NULL)
                <div class="col-sm-7 col-md-6 col-lg-5">
                    <p>Maaf Tidak Ada Produk.</p>
                </div>
                @else
                <div class="col-sm-7 col-md-6 col-lg-5">
                    <figure class="figure">
                        <img src="{{ asset('storages/'. $produk->image) }}" class="figure-img img-fluid img-thumbnail">
                        <figcaption class="figure-caption product-thumbnail-container d-flex justify-content-between">
                            @foreach ($produk->produkimage as $item)
                            <a href="">
                                <img src="{{ asset('storages/'. $item->image_path) }}" alt="{{ $produk->nama_produk }}">
                            </a>
                            @endforeach
                        </figcaption>
                    </figure>
                </div>
                <div class="col-sm-5 col-md-6 col-lg-4">
                    <h3>{{ $produk->nama_produk }}</h3>
                    <h5>Rp. {{ number_format($produk->harga_jual,2,',','.') }}</h5>
                    
                    {{-- <button type="button" href="" class="btn btn-sm ml-2"
                        style="background-color: red; color: white;"><i class="fa fa-heart"></i> Favorit</button> --}}
                    <div class="button-product">
                        {{-- <a href="cart.html" class="btn btn-lg"
                            style="background-color: #28BFDB; color: white; font-weight: bold;">Beli
                            sekarang</a> --}}
                        <form action="{{ route('frontend.cart.store') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $produk->id }}">
                            <input type="hidden" name="nama_produk" value="{{ $produk->nama_produk }}">
                            <input type="hidden" name="harga_jual" value="{{ $produk->harga_jual }}">
                            {{-- <button class="add-to-cart btn btn-lg" 
                                    type="submit"
                                    style="background-color: #eaeaea; color: #707070; font-weight: bold;">
                            <i class="fa fa-shopping-cart"></i> Masukkan ke keranjang belanja
                            </button> --}}
                            
                        </form>
                    </div>
                    <hr>
                    <p class="pb-0">Penjual</p>
                    <div class="row">
                        <div class="col-3 col-sm-4 col-md-3 col-lg-3">
                            <img src="{{ asset('assets/image/penjual.png') }}" width="80%">
                        </div>
                        <div class="col-8 col-sm-8 col-md-9 col-lg-9 ">
                            <p class="mb-0" style="font-weight: bold; color: #707070;">{{ $produk->penjual->nama }}</p>
                            <p><i class="fa fa-whatsapp" aria-hidden="true"></i><span> {{ $produk->penjual->nohp }}</span></p>
                        </div>
                    </div>
                    <a target="_blank" href="https://api.whatsapp.com/send?phone={{ $produk->penjual->nohp }}&text=Min, Saya ingin beli produk {{$produk->nama_produk}} dong" class="btn btn-lg"
                        style="background-color: #23D43A; color: white; font-weight: bold;">
                        <i class="fa fa-phone"></i> Hubungi Penjual</a>
                </div>
                @endif
            </div>
        </div>
    </section>

    <section class="product-description pt-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-deskripsi-tab" data-toggle="tab"
                                href="#nav-deskripsi" role="tab" aria-controls="nav-deskripsi"
                                aria-selected="true">Deskripsi barang</a>
                            {{-- <a class="nav-item nav-link" id="nav-review-tab" data-toggle="tab" href="#nav-review"
                                role="tab" aria-controls="nav-review" aria-selected="false">Review</a> --}}
                        </div>
                    </nav>
                    <div class="tab-content p-3" id="nav-tabContent">
                        <div class="tab-pane fade show active product-deskripsi overflow-auto" id="nav-deskripsi"
                            role="tabpanel" aria-labelledby="nav-deskripsi-tab">
                            {!! ($produk != NULL ) ? $produk->deskripsi : '' !!}
                        </div>
                        {{-- <div class="tab-pane fade product-review overflow-auto" id="nav-review" role="tabpanel"
                            aria-labelledby="nav-review-tab">
                            <div class="row mb-3">
                                <div class="col-2 col-md-1">
                                    <img src="image/produkDesa/single/pembeli.png" alt="pembeli">
                                </div>
                                <div class="col-10 col-md-9">
                                    <h5>Contoh Pembeli pertama</h5>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere odio nam non
                                        dolorem itaque minus.</p>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-2 col-md-1">
                                    <img src="image/produkDesa/single/pembeli.png" alt="pembeli">
                                </div>
                                <div class="col-10 col-md-9">
                                    <h5>Contoh Pembeli kedua</h5>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere odio nam non
                                        dolorem itaque minus.</p>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-2 col-md-1">
                                    <img src="image/produkDesa/single/pembeli.png" alt="pembeli">
                                </div>
                                <div class="col-10 col-md-9">
                                    <h5>Contoh Pembeli ketiga</h5>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere odio nam non
                                        dolorem itaque minus.</p>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-2 col-md-1">
                                    <img src="image/produkDesa/single/pembeli.png" alt="pembeli">
                                </div>
                                <div class="col-10 col-md-9">
                                    <h5>Contoh Pembeli keempat</h5>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere odio nam non
                                        dolorem itaque minus.</p>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-2 col-md-1">
                                    <img src="image/produkDesa/single/pembeli.png" alt="pembeli">
                                </div>
                                <div class="col-10 col-md-9">
                                    <h5>Contoh Pembeli kelima</h5>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere odio nam non
                                        dolorem itaque minus.</p>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-2 col-md-1">
                                    <img src="image/produkDesa/single/pembeli.png" alt="pembeli">
                                </div>
                                <div class="col-10 col-md-9">
                                    <h5>Contoh Pembeli keenam</h5>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere odio nam non
                                        dolorem itaque minus.</p>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-2 col-md-1">
                                    <img src="image/produkDesa/single/pembeli.png" alt="pembeli">
                                </div>
                                <div class="col-10 col-md-9">
                                    <h5>Contoh Pembeli keenam</h5>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere odio nam non
                                        dolorem itaque minus.</p>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="produk-terkait">
        <div class="container">
            <h5 class="mb-4 " style="font-weight: bold; font-size: 22px;">Pencarian Yang Terkait</h5>
            <div class="row">

                @if($produkterkait == NULL)
                <div class="col-sm-7 col-md-6 col-lg-5">
                    <p>Maaf Tidak Ada Produk Terkait.</p>
                </div>
                @else
                @foreach($produkterkait as $item)
                <div class="col-6 col-sm-6 col-md-3 col-lg-3">
                    <figure class="figure">
                        <div class="figure-img">
                            <img src="{{ asset('storages/'. $item->image) }}" alt="produk2" style="width: 100%">
                            <a href="{{ route('frontend.show', $item->slug) }}" class="d-flex justify-content-center">
                                <img src="{{ asset('assets/image/produkDesa/badge.png') }}" class="align-self-center" style="width: 15%">
                            </a>
                        </div>
                        <figcaption class="figure-caption text-center">
                            <h5 class="mb-0">{{ $item->nama_produk }}</h5>
                            <p>Rp. {{ number_format($item->harga_jual,2,',','.') }}</p>
                        </figcaption>
                    </figure>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </section>

@endsection
@extends('frontend.template.default')

@section('content')

    <div class="container">
        <div class="display-all">
            <div class="row">
                <div class="col align-self-center">
                    <h1 style="font-weight:bold;">Peralatan Rumah Tangga</h1>
                </div>
                <div class="col align-self-center">
                    <hr style=" border: 2px solid grey; border-radius: 10px;">
                </div>
            </div>
        </div>
    </div>
    <!-- End of Display Center -->

    <!-- Breadcrum -->
    <div class="container mt-3">
        <nav>
            <ol class="breadcrumb breadcrumb-all">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="">Search</a></li>
            </ol>
        </nav>
    </div>
    <!-- Akhir Breadcrum -->

    <!-- All Produk -->
    <section class="produk-desa mb-3">
        <div class="container">
            <div class="item-produk text-center">
                <div class="row">
                    @foreach($search as $item)
                    <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                        <figure class="figure">
                            <div class="figure-img">
                                <img src="{{ asset('storages/'. $item->image) }}" alt="prdouk1" class="img-thumbnail">
                                <a href="{{ route('frontend.show', $item->slug) }}" class="d-flex justify-content-center">
                                    <img src="{{ asset('assets/image/produkDesa/badge.png') }}" class="align-self-center" style="width: 25%">
                                </a>
                            </div>
                            <figcaption class="figure-caption text-center">
                                <h5 class="mb-0">{{ $item->nama_produk }}</h5>
                                <p class="pt-0">Rp. {{ number_format($item->harga_jual,2,',','.') }}</p>
                            </figcaption>
                        </figure>
                    </div>
                    @endforeach
                </div>
                <div style="text-align: center;">{{ $search->links() }}</div>
            </div>
        </div>
    </section>


    <!-- End Produk -->

@endsection
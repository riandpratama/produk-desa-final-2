@extends('adminlte::page')

@section('title', 'Produk Desa - Pemesanan')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.12/css/bootstrap-select.css" />

@section('content')
	@if (Session::has('success'))
        <div class="alert alert-success alert-hidden" role="alert">
            <strong>Success</strong>{{ Session::get('success') }}
        </div>
    @endif
    <section class="content">
    	<div class="row">	
			<div class="col-lg-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h2 class="box-title"><b>Halaman Pemesanan</b></h2>
					</div>
					<div class="box-body">

	                    <div class="box-body table-responsive no-padding">
	                        <table class="table table-hover" id="datatable">
	                            <thead>
	                                <tr>
	                                    <th width="8%">No</th>
	                                    <th>Kode Pemesanan</th>
	                                    <th>Nama Pelanggan</th>
	                                    <th>Tanggal Pemesanan</th>
	                                    <th>Status</th>
	                                    <th width="18%">Aksi</th>
	                                </tr>
	                            </thead>
	                            <tfoot>
	                                <tr>
	                                    <th>No</th>	                                    
	                                    <th>Kode Pemesanan</th>
	                                    <th>Nama Pelanggan</th>
	                                    <th>Tanggal Pemesanan</th>
	                                    <th>Aksi</th>
	                                </tr>
	                            </tfoot>
	                            <tbody>
	                            	@foreach($pemesanan as $item)
	                                <tr>
	                                    <td>{{ $loop->iteration }}.</td>
	                                    <td>{{ $item->kode_pemesanan }}</td>
	                                    <td>{{ $item->user->name }}</td>
	                                    <td>{{ date('d/m/Y', strtotime($item->tgl_pemesanan)) }}</td>
	                                    <td>
											@if($item->status == 0)
			                                   <span style="font-size: 12px" class="label label-warning">Menunggu Verifikasi</span>
			                                @elseif ($item->status == 2)
			                                    <span style="font-size: 12px" class="label label-danger">Ditolak</span>
			                                @else
			                                    <span style="font-size: 12px" class="label label-success">Telah Terverifikasi</span>
			                                @endif
	                                    </td>
	                                    <td>
	                                    	@if ($item->status == 0)
	                                    	<button class="btn btn-info btn-xs" data-title="Edit" data-toggle="modal" data-target="#approvdata" onclick="approvdata(this);" data-item="{{$item}}" data-backdrop="static" data-keyboard="false"><span class="glyphicon glyphicon-ok-sign"></span></button>
											@endif
	                                    	<button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" onclick="detail(this);" data-item="{{$item}}" data-backdrop="static" data-keyboard="false"><span class="glyphicon glyphicon-list-alt icon-list-alt"></span></button>
	                                    	
	                                    	<p data-placement="top" data-toggle="tooltip" title="Delete" style="display:inline-block;"> 
	                                    		<form action="{{ route('penjual.destroy', $item->id) }}" method="GET" style="display:inline-block;">
                                             		<button title="Delete" class="btn btn-danger js-submit-confirm btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span> </button>
                                        		</form>
                                            </p>
	                                    </td>
	                                </tr>
	                                @endforeach
	                            </tbody>
	                        </table>
	                    </div>
					</div>
				</div>
			</div>
    	</div>
    </section>

	<!-- #START# MODAL -->
     <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    	<div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
	          	<div class="modal-header">
	          		<h4><i class="fa fa-eye"></i> Lihat Detail Pemesanan</h4>
		        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		     	</div>
		        <div class="modal-body">
		          	<form id="formEdit" method="POST" class="formedit">
		          		@csrf
		          		<div class="col-md-6">
			                <div class="form-group{{ $errors->has('kode_pemesanan') ? ' has-error' : '' }}">
			                    <label for="kode_pemesanan">Kode Pemesanan</label>
			                    <input id="kode_pemesanan" type="text" class="form-control" name="kode_pemesanan" value="{{ old('kode_pemesanan') }}" readonly>
			                    @if ($errors->has('kode_pemesanan'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('kode_pemesanan') }}</strong>
			                        </span>
			                    @endif
			                </div>

			      	  		<div class="form-group{{ $errors->has('nama_pelanggan') ? ' has-error' : '' }}">
			                    <label for="nama_pelanggan">Nama Pelanggan</label>
			                    <input id="nama_pelanggan" type="text" class="form-control" name="nama_pelanggan" value="{{ old('nama_pelanggan') }}" readonly>
			                    @if ($errors->has('nama_pelanggan'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('nama_pelanggan') }}</strong>
			                        </span>
			                    @endif
			                </div>

			                <div class="form-group{{ $errors->has('tgl_pemesanan') ? ' has-error' : '' }}">
			                    <label for="tgl_pemesanan">Tanggal Pemesanan</label>
			                    <input id="tgl_pemesanan" type="date" class="form-control" name="tgl_pemesanan" value="{{ old('tgl_pemesanan') }}" readonly>
			                    @if ($errors->has('tgl_pemesanan'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('tgl_pemesanan') }}</strong>
			                        </span>
			                    @endif
			                </div>

			                <div class="form-group{{ $errors->has('total_pesan') ? ' has-error' : '' }}">
			                    <label for="total_pesan">Total Pemesanan</label>
			                    <input id="total_pesan" type="text" class="form-control" name="total_pesan" value="{{ old('total_pesan') }}" readonly>
			                    @if ($errors->has('total_pesan'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('total_pesan') }}</strong>
			                        </span>
			                    @endif
			                </div>

			                <div class="form-group{{ $errors->has('bayar_unik') ? ' has-error' : '' }}">
			                    <label for="bayar_unik">Bayar Unik</label>
			                    <input id="bayar_unik" type="text" class="form-control" name="bayar_unik" value="{{ old('bayar_unik') }}" readonly>
			                    @if ($errors->has('bayar_unik'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('bayar_unik') }}</strong>
			                        </span>
			                    @endif
			                </div>

			                <div class="form-group{{ $errors->has('bank') ? ' has-error' : '' }}">
			                    <label for="bank">Bank Transfer</label>
			                    <input id="bank" type="text" class="form-control" name="bank" value="{{ old('bank') }}" readonly>
			                    @if ($errors->has('bank'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('bank') }}</strong>
			                        </span>
			                    @endif
			                </div>
		                </div>
		                <div class="col-md-6">
		                	<div class="form-group{{ $errors->has('provinsi') ? ' has-error' : '' }}">
			                    <label for="provinsi">Provinsi</label>
			                    <input id="provinsi" type="text" class="form-control" name="provinsi" value="{{ old('provinsi') }}" readonly>
			                    @if ($errors->has('provinsi'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('provinsi') }}</strong>
			                        </span>
			                    @endif
			                </div>

			      	  		<div class="form-group{{ $errors->has('kabupaten') ? ' has-error' : '' }}">
			                    <label for="kabupaten">Kota/Kabupaten</label>
			                    <input id="kabupaten" type="text" class="form-control" name="kabupaten" value="{{ old('kabupaten') }}" readonly>
			                    @if ($errors->has('kabupaten'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('kabupaten') }}</strong>
			                        </span>
			                    @endif
			                </div>

			                <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
			                    <label for="alamat">Alamat Pengiriman</label>
			                    <input id="alamat" type="text" class="form-control" name="alamat" value="{{ old('alamat') }}" readonly>
			                    @if ($errors->has('alamat'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('alamat') }}</strong>
			                        </span>
			                    @endif
			                </div>

			                <div class="form-group{{ $errors->has('kurir') ? ' has-error' : '' }}">
			                    <label for="kurir">Kurir Pengiriman</label>
			                    <input id="kurir" type="text" class="form-control" name="kurir" value="{{ old('kurir') }}" readonly>
			                    @if ($errors->has('kurir'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('kurir') }}</strong>
			                        </span>
			                    @endif
			                </div>

			                <div class="form-group{{ $errors->has('ongkos_kirim') ? ' has-error' : '' }}">
			                    <label for="ongkos_kirim">Ongkos Pengiriman</label>
			                    <input id="ongkos_kirim" type="text" class="form-control" name="ongkos_kirim" value="{{ old('ongkos_kirim') }}" readonly>
			                    @if ($errors->has('ongkos_kirim'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('ongkos_kirim') }}</strong>
			                        </span>
			                    @endif
			                </div>

			                <div class="form-group{{ $errors->has('service_kurir') ? ' has-error' : '' }}">
			                    <label for="service_kurir">Service Kurir Pengiriman</label>
			                    <input id="service_kurir" type="text" class="form-control" name="service_kurir" value="{{ old('service_kurir') }}" readonly>
			                    @if ($errors->has('service_kurir'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('service_kurir') }}</strong>
			                        </span>
			                    @endif
			                </div>
		                </div>

		                <div class="col-md-12">
		                	<table class="table" id="myTable">
		                		<thead>
		                		<tr style="background-color: #605ca8; color: white;">
		                			<td>No</td>
		                			<td>Nama Penjual</td>
		                			<td>Nama Produk</td>
		                			<td>Jumlah</td>
		                			<td>Harga Total Produk</td>
		                		</tr>
		                		</thead>
		                		<tbody>
		                		</tbody>
			            	</table>
			            </div>
		                <div class="modal-footer">
		                	{{-- <button type="submit" class="btn btn-primary " onclick="return confirm('Anda yakin ingin memverifikasi?')"><span class="glyphicon glyphicon-ok-sign"></span> Verifikasi</button> --}}
		        			<button type="button" class="btn btn-danger " data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Close</button>
		      	  		</div>
		            </form>
		        </div>
		    </div>
		</div>
	</div>
	<!-- #END# MODAL -->

	{{--modal approval--}}
	
	<div class="modal fade" id="modalApprov" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title updatePJP" id="exampleModalLabel"><i class="fa fa-check"></i> Verifikasi Pemesanan</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <form method="post" id="formapprov" enctype="multipart/form-data">
	                {{ csrf_field() }}
	                @csrf
	                <input type="hidden" id="id_approv" name="id">
	                <div role="content">
	                    <div class="widget-body">
	                        <div class="row">
	                            <div class="modal-body">
	                                <div class="col-sm-12">
	                                    <div class="form-group">
	                                        <label> Pilih Verifikasi :</label>
	                                        <div class="input-group">
	                                            <span class="input-group-addon "><i class="fa fa-info-circle fa-fw"></i></span>
	                                            <select name="status" id="approv" class="form-control" >
	                                                <option value="1">Verifikasi</option>
	                                                <option value="2">Tolak</option>
	                                            </select>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="modal-footer">
	                                <div class="col-sm-12">
	                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
	                                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Verifikasi</button>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </form>
	        </div>
	    </div>
	</div>
	
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.12/js/bootstrap-select.js"></script>
<script type="text/javascript">
  	$(document).ready(function() {
	    $('#datatable').dataTable();
	} );

	$("document").ready(function(){
        setTimeout(function(){
            $(".alert-hidden").remove();
        }, 7000 ); 
    });
</script>

<script>
    function detail(button){
        
        var item = $(button).data('item');
        
        // Menghitung Rupiah
        var bilangan = item.ongkos_kirim;
		var	number_string = bilangan.toString(),
			sisa 	= number_string.length % 3,
			rupiah 	= number_string.substr(0, sisa),
			ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
				
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

        $('form#formEdit').attr('action','{{ url("backend/pemesanan") }}/'+item.id+'/update');
        $('#formEdit .form-group #kode_pemesanan').val(item.kode_pemesanan);
        $('#formEdit .form-group #nama_pelanggan').val(item.user.name);
        $('#formEdit .form-group #tgl_pemesanan').val(item.tgl_pemesanan);

        $('#formEdit .form-group #provinsi').val(item.provinsi);
        $('#formEdit .form-group #kabupaten').val(item.kabupaten);
        $('#formEdit .form-group #alamat').val(item.alamat_kirim);

        $('#formEdit .form-group #total_pesan').val(item.total_pesan);
        $('#formEdit .form-group #bayar_unik').val(item.bayar_unik);
        $('#formEdit .form-group #bank').val(item.bank);

        $('#formEdit .form-group #kurir').val(item.kurir);
        $('#formEdit .form-group #ongkos_kirim').val(rupiah);
        $('#formEdit .form-group #service_kurir').val(item.service_kurir);

        const pemesanandetail = item.pemesanandetail;
        
        if (pemesanandetail.length > 0) {

        	var jumlah_barang = '';

	        for(var awal = 0; awal < pemesanandetail.length; awal++){
	        	var no = awal+1;
	        	nama_produk = pemesanandetail[awal].produk.nama_produk;
	        	jumlah_barang = pemesanandetail[awal].jumlah_barang;
	        	harga_total_barang = pemesanandetail[awal].harga_total_barang;

	        	nama_penjual = pemesanandetail[awal].produk.penjual.nama;
	        	
	        	var detailproduk = '<tr><td>'+ no +'.</td><td>'+ nama_penjual +'</td><td>'+nama_produk+'</td> <td>'+jumlah_barang+'</td><td>Rp. '+harga_total_barang+'</td></tr>';

	        	$('#myTable > tbody:last-child').append(detailproduk);
	        };

	    } else {
	    	
	    }

	    $('button[data-dismiss=modal]').click(function () {
	        $('#myTable > tbody:last-child').html('');
	    });
    }

    function approvdata(obj) {
        var item = $(obj).data('item');
        $('form#formapprov').attr('action','{{ url("backend/pemesanan/verifikasi") }}');
        $('#modalApprov').modal('show');
        $('#id_approv').val(item.id);
    }

</script>

@endsection
@extends('adminlte::page')

@section('title', 'Produk Desa - Jenis')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/css/lightbox.css">

@section('content')

	<section class="content">
        <div class="row">   
            <div class="col-lg-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h2 class="box-title"><b> Halaman Produk Foto</b></h2>
                    </div>
                    <div class="box-body">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    DETAIL PRODUK
                                    <small>Semua Foto berada disini.</small>
                                </h2>
                                <br>
                                <a href="{{ route('produk.edit', $produk->id) }}" class="btn btn-success">Kembali</a>
                            </div>
                            <div class="body">
                                <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                    @foreach ($produk->produkimage as $item)

                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <form action="{{ route('produk.destroyDetailProduk', $item->id) }}" method="GET" style="display:inline-block;">
                                            <a href="{{ asset('storages/'.$item->image_path) }}" data-lightbox="mygallery" >
                                                <img src="{{ asset('storages/'.$item->image_path) }}" class="img-responsive" >
                                            </a>
                                        
                                            <button title="Delete" class="btn btn-danger btn-block" type="submit">(x) Hapus</button>
                                        </form>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <hr>
                            <div class="body">
                                <div class="table-responsive">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Masukkan Gambar</div>

                                            <div class="panel-body">
                                                <form class="dropzone" action="{{ route('produk.storeDetailProduk') }}" method="post" id="addImages" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="produk_id" onclick="return confirm('Anda yakin ingin menghapus?')" value="{{ $produk->id }}">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/js/lightbox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>
    <script type="text/javascript">
        Dropzone.options.addImages = {
            maxFilesize: 2,
            acceptedFiles: 'image/*',
            success: function(file, response) {
                if (file.status == 'success') {
                    handleDropzoneFileUpload.handleSuccess(response);
                } else {
                    handleDropzoneFileUpload.handleError(response);
                }
            }
        };

        var handleDropzoneFileUpload = {
            handleError: function(response) {
                console.log(response);
            },
            handleSuccess: function(response) {
                var imageList = $('#aniimated-thumbnials');
                var imageSrc = '{{ asset('storages') }}' + '/' + response.image_path;

                $(imageList).append('<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> <form action="" method="GET" style="display:inline-block;"><a href="' + imageSrc + '" data-lightbox="mygallery" ><img src="' + imageSrc + '" class="img-responsive" ></a> </div>');
            }
        };
    </script>
@stop
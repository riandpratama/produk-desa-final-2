@extends('adminlte::page')

@section('title', 'Produk Desa - Jenis')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.12/css/bootstrap-select.css" />

@section('content')

    <section class="content">
    	<div class="row">	
			<div class="col-lg-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h2 class="box-title"><b>Halaman Produk</b></h2>
					</div>
					<div class="box-body">
						
						<a href="{{ route('produk.create') }}" class="btn btn-primary waves-effect m-r-20" style="margin-bottom: 15px;"><i class="fa fa-plus"></i>&nbsp;Tambah Produk </a>

	                    <div class="box-body table-responsive no-padding">
	                        <table class="table table-hover" id="datatable">
	                            <thead>
	                                <tr>
	                                    <th>No</th>
	                                    <th>Jenis</th>
	                                    <th>Nama Produk</th>
	                                    <th>Harga</th>
	                                    <th>Stok</th>
	                                    <th>Aksi</th>
	                                </tr>
	                            </thead>
	                            <tfoot>
	                                <tr>
	                                    <th>No</th>
	                                    <th>Jenis</th>
	                                    <th>Nama Produk</th>
	                                    <th>Harga</th>
	                                    <th>Stok</th>
	                                    <th>Aksi</th>
	                                </tr>
	                            </tfoot>
	                            <tbody>
	                            	@foreach($produk as $item)
	                                <tr>
	                                    <td>{{ $loop->iteration }}.</td>
	                                    <td>{{ $item->jenis->name }}</td>
	                                    <td>{{ $item->nama_produk }}</td>
	                                    <td>Rp. {{ number_format($item->harga_jual) }}</td>
	                                    <td>{{ number_format($item->stok) }}</td>
	                                    <td>
	                                    	<a href="{{ route('produk.edit', $item->id) }}" class="btn btn-warning waves-effect m-r-20" >EDIT</a>
	                                    	
	                                    	<form action="{{ route('produk.destroy', $item->id) }}" method="GET" style="display:inline-block;">
	                                             <button type="submit" class="btn btn-danger waves-effect m-r-20" data-toggle="modal" onclick="return confirm('Anda yakin ingin menghapus?')">HAPUS</button>
	                                        </form>
	                                    </td>
	                                </tr>
	                                @endforeach
	                            </tbody>
	                        </table>
	                    </div>
					</div>
				</div>
			</div>
    	</div>
    </section>

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.12/js/bootstrap-select.js"></script>
<script type="text/javascript">
  	$(document).ready(function() {
	    $('#datatable').dataTable();
	} );
</script>


@endsection
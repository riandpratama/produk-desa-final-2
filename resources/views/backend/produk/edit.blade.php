@extends('adminlte::page')

@section('title', 'Produk Desa - Jenis')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.12/css/bootstrap-select.css" />

@section('content')

    <section class="content">
    	<div class="row">	
			<div class="col-lg-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h2 class="box-title"><b>Edit Halaman Produk</b></h2>
					</div>
					<div class="box-body">
						<form class="form-horizontal" action="{{ route('produk.update', $produk->id) }}" method="POST" enctype= multipart/form-data>
	                    @csrf

	                    <div class="clearfix">
	                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="email_address_2">Jenis Produk : </label>
	                        </div>
	                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                            <div class="form-group">
	                                <div class="form-line">
	                                    <select class="form-control show-tick" name="jenis_id" id="kategori" required="">
	                                    	@foreach ($jenis as $item)
	                                        <option value="{{ $item->id }}" @if($produk->jenis_id == $item->id) selected="" @endif>{{ $item->name }}</option>
	                                        @endforeach
	                                    </select>
	                                </div>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="clearfix">
	                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="">Nama Produk : </label>
	                        </div>
	                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                            <div class="form-group">
	                                <div class="form-line">
	                                    <input type="text" name="nama_produk" id="" class="form-control" placeholder="Masukkan Nama Produk" required="" value="{{ old('nama_produk') ? old('nama_produk') : $produk->nama_produk }}">
	                                </div>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="clearfix">
	                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="">Harga Jual : </label>
	                        </div>
	                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                            <div class="form-group">
	                                <div class="form-line">
	                                    <input type="text" name="harga_jual" id="harga_jual" class="harga_jual form-control" placeholder="Masukkan Harga Penjualan Produk" required=""  value="{{ old('harga_jual') ? old('harga_jual') : number_format($produk->harga_jual,0,',','.') }}">
	                                </div>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="clearfix">
	                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="stok">Jumlah Stok : </label>
	                        </div>
	                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                            <div class="form-group">
	                                <div class="form-line">
	                                    <input type="number" name="stok" id="stok" class="stok form-control" placeholder="Masukkan Jumlah Stok Produk" required="" value="{{ old('stok') ? old('stok') : number_format($produk->stok,0,',','.') }}">
	                                </div>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="clearfix">
	                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="">Keterangan Produk : </label>
	                        </div>
	                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                            <div class="form-group">
	                                <div class="form-line">
	                                    <textarea name="deskripsi" class="form-control" required="">{{ old('deskripsi') ? old('deskripsi') : $produk->deskripsi }}</textarea>
	                                </div>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="clearfix">
	                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="">Foto Produk Sampul : </label>
	                        </div>
	                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                            <div class="form-group">
	                                <div class="form-line">
	                                    <input type="file" name="image" class="form-control">
	                                </div>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="clearfix">
	                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="">Foto Produk : </label>
	                        </div>
	                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                            <div class="form-group">
	                                <a href="{{ route('produk.detailproduk', $produk->id) }}" class="btn btn-success">Klik Detail Produk</a>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="clearfix">
	                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
	                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">UPDATE</button>

	                            <a href="{{ route('produk.index') }}" class="btn btn-danger m-t-15 waves-effect">BATAL</a>
	                        </div>
	                    </div>
	                </form>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection

@section('js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.12/js/bootstrap-select.js"></script>

<script src="{{ asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script>
    $('textarea').ckeditor({
        "filebrowserImageBrowseUrl" : '/laravel-filemanager?type=Images',
        "filebrowserBrowseUrl" : "/laravel-filemanager?type=Files"
    });
</script>


<script type="text/javascript">
    function currency(number) {
        var rupiah = '',
            input = parseInt(number.replace(/[^0-9]/g, ''), 10),
            currency = input.toString().split('').reverse().join('');

        for(var i = 0; i < currency.length; i++) if(i%3 == 0) rupiah += currency.substr(i,3)+'.';

        return rupiah.split('',rupiah.length-1).reverse().join('');
    }

    function implodeCurrency(currency) {
        var curr = '';

        for (var i = 0; i < currency.length; i++) curr += currency[i];

        return parseInt(curr);
    }

    $('.harga_awal').keyup(function(event) {
        var realRupiah = $(this).val();

        document.getElementById('harga_awal').value = currency(event.target.value);
    });

    $('.harga_jual').keyup(function(event) {
        var realRupiah = $(this).val();

        document.getElementById('harga_jual').value = currency(event.target.value);
    });

    $('.stok').keyup(function(event) {
        var realRupiah = $(this).val();

        document.getElementById('stok').value = currency(event.target.value);
    });
</script>

@endsection
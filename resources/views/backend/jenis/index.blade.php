@extends('adminlte::page')

@section('title', 'Produk Desa - Jenis')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.12/css/bootstrap-select.css" />

@section('content')

    <section class="content">
    	<div class="row">	
			<div class="col-lg-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h2 class="box-title"><b>Halaman Jenis Produk</b></h2>
					</div>
					<div class="box-body">
						
						<a href="" class="btn btn-primary waves-effect m-r-20" style="margin-bottom: 15px;" data-title="Tambah" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i>&nbsp;Tambah Jenis </a>

	                    <div class="box-body table-responsive no-padding">
	                        <table class="table table-hover" id="datakurikulum">
	                            <thead>
	                                <tr>
	                                    <th width="8%">No</th>
	                                    <th>Kategori</th>
	                                    <th>Jenis</th>
	                                    <th width="18%">Aksi</th>
	                                </tr>
	                            </thead>
	                            <tfoot>
	                                <tr>
	                                    <th>No</th>	                                    
	                                    <th>Kategori</th>
	                                    <th>Jenis</th>
	                                    <th>Aksi</th>
	                                </tr>
	                            </tfoot>
	                            <tbody>
	                            	@foreach($jenis as $item)
	                                <tr>
	                                    <td>{{ $loop->iteration }}.</td>
	                                    <td>{{ $item->kategori }}</td>
	                                    <td>{{ $item->name }}</td>
	                                    <td>
	                                    	<p data-placement="top" data-toggle="tooltip" title="Edit" style="display:inline-block;" ><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" onclick="insertFormEdit(this);" data-item="{{$item}}"><span class="glyphicon glyphicon-pencil"></span></button></p>
	                                    	
	                                    	<p data-placement="top" data-toggle="tooltip" title="Delete" style="display:inline-block;"> 
	                                    		<form action="{{ route('jenis.destroy', $item->id) }}" method="GET" style="display:inline-block;">
                                             		<button title="Delete" class="btn btn-danger js-submit-confirm btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span> </button>
                                        		</form>
                                            </p>
	                                    </td>
	                                </tr>
	                                @endforeach
	                            </tbody>
	                        </table>
	                    </div>
					</div>
				</div>
			</div>
    	</div>
    </section>

    <!-- #START# MODAL -->
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
	          <div class="modal-header">
	          	<h4><i class="fa fa-plus"></i> Tambah data</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		      </div>
		          <div class="modal-body">
		          	<form action="{{ route('jenis.store') }}" method="POST" enctype="multipart/form-data">
	              		{{ csrf_field() }}
	              		<div class="form-group{{ $errors->has('kategori') ? ' has-error' : '' }}">
		                    <label for="kategori">Kode Labeling</label>
		                    <select name="kategori" class="selectpicker form-control" data-live-search="true">
		                    	<option selected="" disabled="">Pilih Kategori</option>
	                            <option value="Kategori Produk">Kategori Produk</option>
	                            <option value="Pertambakan">Pertambakan</option>
	                            <option value="Kreasi Desa">Kreasi Desa</option>
	                        </select>
		                    @if ($errors->has('kategori'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('kategori') }}</strong>
		                        </span>
		                    @endif
		                </div>

		                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		                    <label for="name">Jenis</label>
		                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>
		                    @if ($errors->has('name'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('name') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                
		                <div class="modal-footer">
		        			<button type="submit" class="btn btn-primary btn-lg" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-save"></span> Simpan</button>
		        			<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		      	  		</div> 
		      	  	</form>
		      	  </div>
	        </div>
	    <!-- /.modal-content --> 
	  	</div>
	      <!-- /.modal-dialog modal-dialog-centered --> 
	</div>
	<!-- #END# MODAL -->

	<!-- #START# MODAL -->
     <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    	<div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
	          	<div class="modal-header">
	          		<h4><i class="fa fa-edit"></i> Edit data</h4>
		        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		     	</div>
		        <div class="modal-body">
		          	<form id="formEdit" method="POST">
		          		@csrf
		          		<div class="form-group{{ $errors->has('kategori') ? ' has-error' : '' }}">
		                    <label for="kategori">Kategori</label>
		                    {{-- <input id="kategori_edit" type="text" class="form-control" name="kategori" value="{{ old('kategori') }}" autofocus> --}}
		                    <select name="kategori" class="selectpicker form-control" data-live-search="true">
		                    	<option value="Kategori Produk">Kategori Produk</option>
	                            <option value="Pertambakan">Pertambakan</option>
	                            <option value="Kreasi Desa">Kreasi Desa</option>
		                    </select>
		                    @if ($errors->has('kategori'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('kategori') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		                    <label for="name">Jenis</label>
		                    <input id="name_edit" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>
		                    @if ($errors->has('name'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('name') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="modal-footer">
		        			<button type="submit" class="btn btn-warning btn-lg" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
		        			<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		      	  		</div>
		            </form>
		        </div>
		    </div>
		</div>
	</div>
	<!-- #END# MODAL -->
@stop

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.12/js/bootstrap-select.js"></script>
<script type="text/javascript">
  	$(document).ready(function() {
	    $('#datakurikulum').dataTable();
	} );
</script>

<script>

    function insertFormEdit(button){
        var item = $(button).data('item');
        console.log(item.kategori)
        $('form#formEdit').attr('action','{{ url("backend/jenis") }}/'+item.id+'/update');
        $('#formEdit .form-group #name_edit').val(item.name);
        $('#formEdit .form-group .bootstrap-select button').attr('title',item.kategori);
        $('#formEdit .form-group .bootstrap-select button .filter-option').html(item.kategori);
        $('#formEdit .form-group .selectpicker option').removeAttr("selected");
    }

</script>

@endsection
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_produk', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kategori')->nullable();
            $table->integer('jenis_id');
            $table->integer('penjual_id');
            $table->string('nama_produk');
            $table->text('deskripsi');
            $table->integer('harga_jual');
            $table->integer('stok');
            $table->text('image');
            $table->string('slug');
            $table->string('reference');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_produk');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_pemesanan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_pemesanan');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('provinsi_id');
            $table->string('provinsi');
            $table->integer('kabupaten_id');
            $table->string('kabupaten');
            $table->text('alamat_kirim')->nullable();
            $table->date('tgl_pemesanan');
            $table->string('kurir')->nullable();
            $table->integer('ongkos_kirim')->nullable();
            $table->string('service_kurir')->nullable();
            $table->boolean('status')->default(0);
            $table->text('upload_bukti')->nullable();
            $table->integer('total_pesan');
            $table->integer('bayar_unik');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanan');
    }
}

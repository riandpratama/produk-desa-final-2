<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemesananDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_pemesanan_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pemesanan_id')->unsigned();
            $table->integer('produk_id')->unsigned();
            $table->integer('jumlah_barang');
            $table->timestamps();

            $table->foreign('pemesanan_id')->references('id')->on('ecommerce_pemesanan')->onDelete('cascade');
            $table->foreign('produk_id')->references('id')->on('ecommerce_produk')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanan_detail');
    }
}

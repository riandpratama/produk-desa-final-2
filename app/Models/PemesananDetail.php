<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PemesananDetail extends Model
{
    protected $table = 'ecommerce_pemesanan_detail';

    protected $guarded = [];

    public function produk()
    {
    	return $this->belongsTo(Produk::class, 'produk_id', 'id');
    }

    public function pemesanan()
    {
    	return $this->belongsTo(Pemesanan::class, 'pemesanan_id');
    }
}

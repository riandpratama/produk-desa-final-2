<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penjual extends Model
{
    protected $table = 'ecommerce_penjual';

    protected $guarded = [];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Produk extends Model
{
    use Sluggable;

    protected $table = 'ecommerce_produk';

    protected $guarded = [];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nama_produk'
            ]
        ];
    }

    public function setReferenceAttribute()
    {
        return $this->attributes['reference'] = '#'.str_random(10);
    }

    public function penjual()
    {
        return $this->belongsTo(Penjual::class, 'penjual_id');
    }

    public function jenis()
    {
        return $this->belongsTo(Jenis::class, 'jenis_id');
    }

    public function produkimage()
    {
        return $this->hasMany(ProdukImage::class, 'produk_id');
    }
}

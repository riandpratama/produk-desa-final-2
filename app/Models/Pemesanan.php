<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pemesanan extends Model
{
    protected $table = 'ecommerce_pemesanan';

    protected $guarded = [];

    public function pemesanandetail()
    {
    	return $this->hasMany(PemesananDetail::class);
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }
}

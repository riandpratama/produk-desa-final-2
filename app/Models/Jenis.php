<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Jenis extends Model
{
    use Sluggable;

    protected $table = 'ecommerce_jenis';

    protected $guarded = [];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function setReferenceAttribute()
    {
        return $this->attributes['reference'] = '#'.str_random(10);
    }

    public function produk()
    {
        return $this->hasMany(Produk::class, 'jenis_id');
    }
}

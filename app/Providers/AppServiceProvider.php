<?php

namespace App\Providers;

use App\Models\Jenis;
use App\Models\Produk;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $jenisKategoriProduk = Jenis::where('kategori', 'Kategori Produk')->get();
        View::share('jenisKategoriProduk', $jenisKategoriProduk);

        $jenisPertambakan = Jenis::where('kategori', 'Pertambakan')->get();
        View::share('jenisPertambakan', $jenisPertambakan);

        $jenisKreasiDesa = Jenis::where('kategori', 'Kreasi Desa')->get();
        View::share('jenisKreasiDesa', $jenisKreasiDesa);
    }
}

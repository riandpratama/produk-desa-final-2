<?php

namespace App\Http\Controllers\Backend;

use Session;
use Storage;
use App\Models\Jenis;
use App\Models\Produk;
use App\Models\Penjual;
use App\Models\ProdukImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::orderBy('created_at', 'DESC')->with('jenis')->get();
        
        return view('backend.produk.index', compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis = Jenis::orderBy('name', 'asc')->get();
        $penjual = Penjual::orderBy('nama', 'asc')->get();

        return view('backend.produk.create', compact('jenis', 'penjual'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('uploads/produk');
        } else {
            $image = NULL;
        }

        $produk = Produk::create([
            'penjual_id' => $request->penjual_id,
            'jenis_id' => $request->jenis_id,
            'reference' => $request->reference,
            'nama_produk' => $request->nama_produk,
            'deskripsi' => $request->deskripsi,
            'harga_jual' => str_replace(',', '', str_replace('.', '',$request->harga_jual)) ?: 0,
            'stok' => $request->stok,
            'image' => $image
        ]);

        $files = $request->file('image_detail');

        if(!empty($files)):
            foreach($files as $file):

                $imageDetail = $file->store('uploads/produk');

                ProdukImage::create([
                    'produk_id' => $produk->id,
                    'image_path' => $imageDetail
                ]);

            endforeach;
        endif;

        return redirect()->route('produk.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::findOrFail($id);

        $jenis = Jenis::orderBy('name', 'asc')->get();

        return view('backend.produk.edit', compact('produk', 'jenis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produk = Produk::findOrFail($id);

        if ($request->file('image') !== null && $request->file('image') !== ''){
            Storage::delete($produk->image);
        }
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('uploads/produk');
        } else {
            $image = $produk->image;
        }

        $produk->slug = null;
        $produk->update([
            'jenis_id' => $request->jenis_id,
            'reference' => $request->reference,
            'nama_produk' => $request->nama_produk,
            'deskripsi' => $request->deskripsi,
            'harga_jual' => str_replace(',', '', str_replace('.', '',$request->harga_jual)) ?: 0,
            'stok' => $request->stok,
            'image' => $image
        ]);

        $files = $request->file('image_detail');
        $produkId = $produk->id;

        if(!empty($files)):

        $gambarDetail = ProdukImage::where(
            'produk_id', $produkId
        )->get()->each(function ($item) use ($request) {

            $imageDetail = $files->store('uploads/produk');

            $item->update([
                'image_path' => $imageDetail[$item->id]
            ]);
        });

        endif;

        return redirect()->route('produk.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Produk::findOrFail($id);
        $detailId = ProdukImage::where('produk_id', $data->id)->get();
        
        Storage::delete($data->image);

        foreach ($detailId as $item):

            Storage::delete($item->image_path);

        endforeach;

        $detail = ProdukImage::where('produk_id', $data->id)->delete();

        $data->delete();

        return redirect()->route('produk.index');
    }

    public function detailproduk($id)
    {
        $produk = Produk::findOrFail($id);

        return view('backend.produk.detailproduk', compact('produk'));
    }

    public function storeDetailProduk(Request $request, Produk $produk)
    {
        $files = $request->file('file');

        if(!empty($files)):
            $produk = Produk::findOrFail($request->produk_id);

            $imageDetail = $files->store('uploads/produk');

            $image = $produk->produkimage()->create([
                'produk_id' => $request->produk_id,
                'image_path' => $imageDetail,
            ]);
        endif;

        return $image;
    }

    public function destroyDetailProduk($id)
    {
        $data = ProdukImage::findOrFail($id);

        Storage::delete($data->image_path);
        $data->delete();

        Session::flash('success', 'The Image has successfully deleted.');

        return redirect()->back();
    }
}

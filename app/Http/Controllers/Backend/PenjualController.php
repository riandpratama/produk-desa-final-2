<?php

namespace App\Http\Controllers\Backend;

use Alert;
use Storage;
use App\Models\Penjual;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PenjualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penjual = Penjual::orderBy('nama', 'asc')->get();

        return view('backend.penjual.index', compact('penjual'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'nohp' => 'required'
        ]);

        Penjual::create([
            'nama' => $request->nama,
            'nohp' => $request->nohp,
        ]);

        Alert::success('Success!');
        
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $penjual = Penjual::find($id);

        $penjual->update([
            'nama' => $request->nama,
            'nohp' => $request->nohp,
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penjual = Penjual::find($id);

        $penjual->delete();

        return redirect()->back();
    }
}

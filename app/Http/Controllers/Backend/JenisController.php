<?php

namespace App\Http\Controllers\Backend;

use Alert;
use Storage;
use App\Models\Jenis;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JenisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jenis = Jenis::orderBy('name', 'asc')->get();

        return view('backend.jenis.index', compact('jenis', 'kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'kategori' => 'required'
        ]);

        Jenis::create([
            'reference' => $request->reference,
            'name' => $request->name,
            'kategori' => $request->kategori,
        ]);

        Alert::success('Success!');
        
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jenis = Jenis::find($id);

        if ($request->file('image_path') !== null && $request->file('image_path') !== ''){
            Storage::delete($jenis->image_path);
        }
        if ($request->hasFile('image_path')) {
            $filename = $request->file('image_path')->store('uploads/jenis');
        } else {
            $filename = $jenis->image_path;
        }
        
        $jenis->slug = null;

        $jenis->update([
            'name' => $request->name,
            'kategori' => $request->kategori,
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jenis = Jenis::find($id);

        $jenis->delete();

        return redirect()->back();
    }
}

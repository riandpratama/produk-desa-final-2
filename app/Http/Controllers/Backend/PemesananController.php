<?php

namespace App\Http\Controllers\Backend;

use Alert;
use Storage;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{Pemesanan, PemesananDetail};

class PemesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pemesanan = Pemesanan::with('user')->with(['pemesanandetail' => function($item){
            $item->with(['produk' => function($penjual){
                $penjual->with('penjual')->get();
            }])->get();
        }])->orderBy('created_at', 'desc')->get();

        return view('backend.pemesanan.index', compact('pemesanan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verifikasi(Request $request)
    {
        $data = [
            'status' => $request->status,
        ];

        $update = Pemesanan::where('id', $request->id)->update($data);

        Session::flash('success', ' Berhasil Verifikasi Data.');

        return redirect()->back();
    }
}

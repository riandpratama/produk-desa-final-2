<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Post Login.
     *
     * @return string
     */
    public function postLogin(Request $request)
    {
        if (Auth::guard()->attempt([
                'email' => $request->email, 
                'password' => $request->password,
                'status' => 1
            ])) 
        {
            return redirect()->intended('/home');        
        } else {
            return redirect()->intended('/login');
        }
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function findUsername()
    {
        $login = request()->input('login');
 
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'no_hp';
 
        request()->merge([$fieldType => $login]);
 
        return $fieldType;
    }

    /**
     * Post Login.
     *
     * @return string
     */
    public function postLoginUser(Request $request)
    {
        if (Auth::guard()->attempt([
                'email' => $request->email, 
                'password' => $request->password,
            ])) 
        {
            return redirect()->intended('/');        
        } else {
            return redirect()->intended('/');
        }
    }
}

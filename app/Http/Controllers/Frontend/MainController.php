<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Jenis;
use App\Models\Produk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = Produk::orderBy('created_at', 'DESC')->get()->take(4);
        $populer = Produk::all()->random()->limit(6)->get();

        return view('frontend.index', compact('news', 'populer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $produk = Produk::where('slug', $slug)->first();
        
        if ($produk == NULL) {
            $produkterkait = NULL;
        } else {
            $produkterkait = Produk::where('jenis_id', $produk->jenis_id)
                            ->where('id', '!=', $produk->id)->get()->take(4);
        }

        return view('frontend.detail-produk', compact('produk', 'produkterkait'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function showJenisProduk(Request $request, $slug)
    {
        $search = $request->get('search');
        $filter = $request->search;

        if (!empty($search) && $filter == 'tertinggi'){
            $jenis = Jenis::where('slug', $slug)->with(['produk' => function($query){
                $query->orderBy('harga_jual', 'desc')->get();
            }])->first();

        } else if(!empty($search) && $filter == 'terendah') {
            $jenis = Jenis::where('slug', $slug)->with(['produk' => function($query){
                $query->orderBy('harga_jual', 'asc')->get();
            }])->first();
        }
        else {
            $jenis = Jenis::where('slug', $slug)->with('produk')->first();
        }
        return view('frontend.jenis-produk', compact('jenis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  string  $search
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {   
        $name = $request->search;

        $search = Produk::where('slug', 'like', '%'.$name.'%')->paginate(6);

        return view('frontend.search', compact('search'));
    }
}

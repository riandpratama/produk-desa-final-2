<?php

namespace App\Http\Controllers\Frontend;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.keranjang');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $duplicates = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id === $request->id;
        });

        if ($duplicates->isNotEmpty()) {
            Session::flash('success_message', ' Produk telah ada di keranjang anda.');
            return view('frontend.keranjang');
        }

        Cart::add($request->id, $request->nama_produk, 1, $request->harga_jual)
            ->associate('App\Models\Produk');

        Session::flash('success_message', ' Produk telah ditambahkan di keranjang anda.');
        return view('frontend.keranjang');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'quantity' => 'required|numeric'
        ]);

        Cart::update($id, $request->quantity);
        session()->flash('success_message', 'Stok berhasil diubah.');
        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);

        Session::flash('success_message', ' Produk telah dihapus dari keranjang anda.');
        return redirect()->back();
    }

}

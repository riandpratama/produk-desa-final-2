<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Auth;
use Carbon;
use Session;
use App\User;
use App\Models\Pemesanan;
use Illuminate\Http\Request;
use App\Models\PemesananDetail;
use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\Facades\Cart;

class CheckoutController extends Controller
{
    public function curl()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: bdc2c9178690de70fad0d5ec4707f40b"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        return json_decode($response, true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Cart::instance('default')->count() == 0) {
            return redirect()->route('frontend.index');
        }

        $data = $this->curl();

        return view('frontend.checkout', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'email' => 'required|unique:users',
            'username' => 'unique:users',
            'nama' => 'required',
            'nohp' => 'required',

            'provinsi_id' => 'required',
            'kabupaten_id' => 'required',
            'alamat' => 'required',
            'kurir' => 'required',
            'ongkos_kirim' => 'required',
            'bank' => 'required',
        ]);

        $generateBayarUnik = rand(2, 300);
        $requestBayar = $request->total_pesan; 
        $total = $requestBayar+$generateBayarUnik;

        $noLastId = DB::table('ecommerce_pemesanan')->max('id');
        $tambah1 = $noLastId+1;

        $date = \Carbon\Carbon::now()->format('Ymd');
        $tgl = \Carbon\Carbon::now()->format('h');

        //Kode pemesanan
        $kodepemesanan = '#' . $date . '-' . $tgl . $tambah1;

        if ($request->password != NULL){
            $user = User::create([
                'name' => $request->nama,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);
        } else {
            $user = Auth::user();
        }

        $pemesanan = Pemesanan::create([
            'kode_pemesanan' => $kodepemesanan,
            'user_id' => $user->id,
            'provinsi_id' => $request->provinsi_id,
            'provinsi' => $request->provinsi,
            'kabupaten_id' => $request->kabupaten_id,
            'kabupaten' => $request->kabupaten,
            'alamat_kirim' => $request->alamat,
            'tgl_pemesanan' => \Carbon\Carbon::now(),
            'kurir' => $request->kurir,
            'ongkos_kirim' => $request->ongkos_kirim,
            'service_kurir' => $request->service_kurir,
            'bank' => $request->bank,
            'total_pesan' => $request->total_pesan,
            'total_barang' => $request->total_barang,
            'bayar_unik' => $total,
            'status' => 0,
        ]);

        foreach ($request->get('produk_id') as $index => $val){

            $detailpemesanan = PemesananDetail::create([
                'pemesanan_id' => $pemesanan->id,
                'produk_id' => $request->produk_id[$index],
                'jumlah_barang' => $request->jumlah_barang[$index],
                'harga_total_barang' => $request->harga_total_barang[$index],
            ]);

        }

        Cart::instance('default')->destroy();

        $pemesananId = $pemesanan->id;

        $pemesanan = Pemesanan::where('id', $pemesananId)->first();

        Session::flash('success_message', ' Invoice.');
        return redirect()->route('frontend.infopembayaran', ['pemesanan' => $pemesanan]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function infopembayaran($id)
    {
        $pemesanan = Pemesanan::findOrFail($id);

        return view('frontend.info-pembayaran', compact('pemesanan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function invoice($id)
    {
        $pemesanan = Pemesanan::findOrFail($id);

        return view('frontend.invoice', compact('pemesanan'));
    }
}
